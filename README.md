# Frontend Mentor - Product preview card component solution

This is a solution to the [Order summary card challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/order-summary-component-QlPmajDUj). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)

## Overview

### The challenge

Users should be able to:

- See hover states for interactive elements

### Screenshot

![](./screenshot.png)

### Links

- Solution URL: [GitLab](https://gitlab.com/savaby-frontendmentor/order-summary-card-challenge)
- Live Site URL: [Vercel](https://order-summary-card-challenge-gray.vercel.app/)

## My process

### Built with

- Pure HTML & CSS
